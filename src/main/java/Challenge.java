
public class Challenge {

    public static void main(String[] args) {

        String sentence = correctSpacing("The film   starts       at      midnight. ");
        System.out.println(sentence);
    }

    public static String correctSpacing(String sentence) {
        return sentence.replaceAll("\\s+", " ").trim();
    }
}
